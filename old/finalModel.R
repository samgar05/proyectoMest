######## Selección Modelo

######## Usamos Backward, Forward y Best Subset

library(car)
library(lmtest)
library(fastDummies)
library(stringr)
library(dplyr)
library(MASS)
library(leaps)
library(MASS)
library(tidyverse)
library(caret)

# importación datos


data <- read.csv("./CarPrice_Assignment.csv", header = TRUE)
head(data)
summary(data)

### Stepwise

df1 <- data
df1 <- df1 %>% dplyr::select(-car_ID)
df1 <- df1 %>% dplyr::select(-CarName)
df1$symboling <- as.factor(df1$symboling)
df1[sapply(df1, is.character)] <- lapply(df1[sapply(df1, is.character)], as.factor)


## Stepwise Aproximación al conjunto de validación 

# split 
set.seed(1)
sample <- sample(c(TRUE, FALSE), nrow(df1), replace=TRUE, prob=c(0.7,0.3))
train  <- df1[sample, ]
test   <- df1[!sample, ]

# Subconjuntos
stepModel <- regsubsets(price~., data = train, nbest=1, nvmax=50, method = "backward")

test.mat <- model.matrix(price ~ ., data = test)

k = length(stepModel)
val.errors <- rep(NA, k)
for ( i in 1:k){
  coefi <- coef(stepModel , id=i)
  pred <- test.mat[ ,names(coefi)] %*% coefi
  val.errors[i] <- mean((test$price - pred)^2)
  }

val.errors
min1 = which.min(val.errors)
val.errors[min1]
plot(val.errors, type = 'b')
points(x=min1,y=val.errors[min1], col='red', pch=3)

stepModel1 <- regsubsets(price~., data = df1, nbest=1, nvmax=48, method = "seqrep")
coef(stepModel1, min1)

## Stepwise Validación cruzada de k-grupos

m <- 10
n <- nrow(df1)
set.seed(1)
folds <- sample(rep(1: m, length=n))
cv.errors <- matrix(NA, m, k, dimnames= list(NULL, paste(1:k)))

predict.regsubsets <- function( object, newdata, id, ...) {
  form <- as.formula(object$call[[2]])
  mat <- model.matrix(form, newdata)
  coefi <- coef(object, id=id)
  xvars <- names(coefi)
  mat [, xvars] %*% coefi
  }

for ( j in 1:m ){
  best.fit <- regsubsets(price ~ ., data=df1[folds != j, ], nvmax=36, method = "seqrep")
  for (i in 1:length(best.fit)){
    pred <- predict.regsubsets(best.fit, df1[ folds==j, ], id=i )
    cv.errors[j, i] <- mean(( df1$price[ folds==j ] - pred )^2)
  }
}

mean.cv.errors <- apply(cv.errors, 2, mean)
mean.cv.errors
par(mfrow = c(1, 1))
plot ( mean.cv.errors, type = "b" )
min2 = which.min(mean.cv.errors)
points(x=min2,y=val.errors[min2], col='red', pch=3)

stepModel2 <- regsubsets(price~., data = df1, nbest=1, nvmax = 36, method = "seqrep")
coef(stepModel2, min2)


### Best Subset

var <- c("price", "enginetype", "fueltype", "carbody", "aspiration",
         "cylindernumber", "drivewheel", "curbweight", "carlength",
         "carwidth", "enginesize", "boreratio", "horsepower", "wheelbase",
         "citympg", "highwaympg")

df2 <- data
df2 <- df2 %>% dplyr::select(all_of(var))
df2[sapply(df2, is.character)] <- lapply(df2[sapply(df2, is.character)], as.factor)


## Validation Set Approach

# split 
set.seed(1)
sample <- sample(c(TRUE, FALSE), nrow(df1), replace=TRUE, prob=c(0.7,0.3))
train  <- df1[sample, ]
test   <- df1[!sample, ]

bestModel1 <- regsubsets(price~., data = train, nbest=1, nvmax = 36, method='exhaustive')

